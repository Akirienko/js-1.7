
let list = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

function workDom(arr){
  let newArr = arr.map(el => `<li>${el}</li>`); 
  let ul = document.createElement("ul");
  newArr.forEach(el => {
    ul.insertAdjacentHTML('beforeend', el)
  });
  document.body.append(ul);
};

workDom(list);



